/*
  ESP8266 BlinkWithoutDelay by Simon Peter
  Blink the blue LED on the ESP-01 module
  Based on the Arduino Blink without Delay example
  This example code is in the public domain

  The blue LED on the ESP-01 module is connected to GPIO1
  (which is also the TXD pin; so we cannot use Serial.print() at the same time)

  Note that this sketch uses LED_BUILTIN to find the pin with the internal LED
*/
#define IR_RX_PIN 14
#define IR_TX_PIN 12
#define SERIAL_BPS 115200

#define POWER_BUTTON 0xFFEA15
#define  EDIT_BUTTON 0xFF7887
#define  EXIT_BUTTON 0xFF38C7
#define   ONE_BUTTON 0xFF08F7
#define   TWO_BUTTON 0xFF8877
#define THREE_BUTTON 0xFF48B7
#define  FOUR_BUTTON 0xFFC837
#define  FIVE_BUTTON 0xFF28D7
#define   SIX_BUTTON 0xFFA857
#define SEVEN_BUTTON 0xFFE817
#define EIGHT_BUTTON 0xFF18E7
#define  NINE_BUTTON 0xFF9867
#define  ZERO_BUTTON 0xFFB847

#include <IRremote.h>

IRrecv irrecv(IR_RX_PIN);
decode_results results;

int ledState = LOW;
volatile byte IRInputState;

void setup() {
    IrReceiver.begin(IR_RX_PIN, ENABLE_LED_FEEDBACK); // Start the receiver
    pinMode(LED_BUILTIN, OUTPUT);
    Serial.begin(SERIAL_BPS);
    Serial.print("herro booted\n");
}

void loop() {
  if (IrReceiver.decode()) {
      Serial.println(IrReceiver.decodedIRData.decodedRawData, HEX);
      IrReceiver.printIRResultShort(&Serial); // optional use new print version
      IrReceiver.resume(); // Enable receiving of the next value
  }
}
