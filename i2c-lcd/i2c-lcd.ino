#define SSD1306_NO_SPLASH

// OLED display width and height, in pixels
#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 32
#define OLED_RESET     -1
// i2c address for oled
///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
#define SCREEN_ADDRESS 0x3C

#include <Adafruit_SSD1306.h>
#include <splash.h>
#include <Adafruit_GrayOLED.h>
#include <gfxfont.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SPITFT.h>
#include <Adafruit_SPITFT_Macros.h>
#include <LiquidCrystal_I2C.h>

// set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

void setup() {
  lcd.init();
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("0x123456789abcde");
  lcd.setCursor(0, 1);
  lcd.print("fghijklmnopqrstu");

  Serial.begin(115200);

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;); // Don't proceed, loop forever
  }

  display.display();
  delay(50); // Pause for 2 seconds

  // Clear the buffer
  display.clearDisplay();

  testdrawstyles();    // Draw 'stylized' characters

  panic();
  //
  //  // Invert and restore display, pausing in-between
  //  display.invertDisplay(true);
  //  delay(1000);
  //  display.invertDisplay(false);
  //  delay(1000);

}

void panic() {
  for (;;) {
    display.invertDisplay(true);
    delay(1000);
  }
}


void loop()
{
}

#define LINE_HEIGHT_PX 9

void testdrawstyles(void) {
  display.clearDisplay();
  display.setTextSize(1);             // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.setCursor(0, 0 * LINE_HEIGHT_PX);
  display.println(F("123456789112345678921"));
  display.setCursor(0, 1 * LINE_HEIGHT_PX);
  display.println(F("234567893123456789412"));
  display.setCursor(0, 2 * LINE_HEIGHT_PX);
  display.println(F("345678951234567896123"));
  display.display();

  display.startscrollright(0x00, 0xFF);



  /*

    display.setTextColor(SSD1306_BLACK, SSD1306_WHITE); // Draw 'inverse' text
    display.println(3.141592);

    display.setTextSize(2);             // Draw 2X-scale text
    display.setTextColor(SSD1306_WHITE);
    display.print(F("0x")); display.println(0xDEADBEEF, HEX);

    display.display();
    delay(2000);
  */
}

/*
  void testscrolltext(void) {
  display.clearDisplay();

  display.setTextSize(1.5); // Draw 2X-scale text
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(10, 0);
  display.println(F("1337CAFE"));
  display.display();      // Show initial text
  delay(70);

  // Scroll in various directions, pausing in-between:
  display.startscrollright(0x00, 0x0F);
  delay(2000);
  display.stopscroll();
  delay(1000);
  display.startscrollleft(0x00, 0x0F);
  delay(2000);
  display.stopscroll();
  delay(1000);
  display.startscrolldiagright(0x00, 0x07);
  delay(2000);
  display.startscrolldiagleft(0x00, 0x07);
  delay(2000);
  display.stopscroll();
  delay(1000);
  }

*/
