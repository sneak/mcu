// Demo the quad alphanumeric display LED backpack kit
// scrolls through every character, then scrolls Serial
// input onto the display

#include <Wire.h>
#include <Adafruit_GFX.h>
#include "Adafruit_LEDBackpack.h"

Adafruit_AlphaNum4 one = Adafruit_AlphaNum4();
Adafruit_AlphaNum4 two = Adafruit_AlphaNum4();

unsigned long ticks;


void setup() {
  Serial.begin(9600);

  one.begin(0x71);  // pass in the address
  two.begin(0x70);

  one.writeDigitRaw(3, 0x0);
  one.writeDigitRaw(0, 0xFFFF);
  one.writeDisplay();
  delay(200);
  one.writeDigitRaw(0, 0x0);
  one.writeDigitRaw(1, 0xFFFF);
  one.writeDisplay();
  delay(200);
  one.writeDigitRaw(1, 0x0);
  one.writeDigitRaw(2, 0xFFFF);
  one.writeDisplay();
  delay(200);
  one.writeDigitRaw(2, 0x0);
  one.writeDigitRaw(3, 0xFFFF);
  one.writeDisplay();
  delay(200);

  one.clear();
  one.writeDisplay();

  two.clear();
  two.writeDisplay();

  one.writeDigitAscii(0, '1');
  one.writeDigitAscii(1, '2');
  one.writeDigitAscii(2, '0');
  one.writeDigitAscii(3, '0');
  two.writeDigitAscii(0, '0');
  two.writeDigitAscii(1, '1');
  two.writeDigitAscii(2, '2');
  two.writeDigitAscii(3, '3');
  one.writeDisplay();
  two.writeDisplay();

    delay(300);

  Serial.println("Start typing to display!");
}


char displaybuffer[4] = {' ', ' ', ' ', ' '};

void loop() {

  ticks = millis();


  sprintf(displaybuffer, "%02d", ticks/1000);
  sprintf(displaybuffer+2, "%02d", ticks-(ticks/1000)*1000);

 Serial.println(displaybuffer);
 
  // set every digit to the buffer
  two.writeDigitAscii(0, displaybuffer[0]);
  two.writeDigitAscii(1, displaybuffer[1]);
  two.writeDigitAscii(2, displaybuffer[2]);
  two.writeDigitAscii(3, displaybuffer[3]);
 
  // write it out!
  two.writeDisplay();
  delay(10);
}
